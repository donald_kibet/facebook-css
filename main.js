var currentSlideShowIndex = 0;
var slide = document.getElementsByClassName('slideshow-item');

setInterval(() => {
    if (currentSlideShowIndex > slide.length - 1) { currentSlideShowIndex = 0 }
    showSlideShow(currentSlideShowIndex);
}, 3000);

function showSlideShow(index) {
    if (index == 0 && slide[slide.length - 1] != null) {
        slide[index].classList.add('active');
        slide[slide.length - 1].classList.remove('active');
    }
    else {
        slide[index].classList.add('active');
        if (index > 0) {
            slide[index - 1].classList.remove('active');
        }
    }
    currentSlideShowIndex++;
}